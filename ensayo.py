from PyPDF2 import PdfFileWriter, PdfFileReader
from io import BytesIO
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
import csv

size=28
ruta= '/generado/'

with open('prueba.csv') as csvarchivo:
    entrada = csv.reader(csvarchivo)
    
    for registro in entrada:
        try:

            regToStr = ' '.join([str(elem) for elem in registro]) 
            Nombre = str(regToStr)

        except HTTPError as e:

            print(e)

        except URLError:

            print("Server down or incorrect domain")

        else:   
                buffer = BytesIO()
                # create a new PDF with Reportlab
                p = canvas.Canvas(buffer, pagesize=A4)
                p.setFont("Helvetica", size)
                p.drawString(50, 300, Nombre)
                p.showPage()
                p.save()

                #move to the beginning of the StringIO buffer
                buffer.seek(0)
                newPdf = PdfFileReader(buffer)

                #######DEBUG NEW PDF created#############
                #pdf1 = buffer.getvalue()
                #open('pdf1.pdf', 'wb').write(pdf1)
                #########################################
                
                # read your existing PDF
                existingPdf = PdfFileReader(open('plantilla.pdf', 'rb'))
                output = PdfFileWriter()
                # add the "watermark" (which is the new pdf) on the existing page
                page = existingPdf.getPage(0)
                page.mergePage(newPdf.getPage(0))
                output.addPage(page)
                # finally, write "output" to a real file
                NombreRuta= '../generado/'+Nombre+'.pdf'                
                outputStream = open(NombreRuta, 'wb')
                print(NombreRuta)
                output.write(outputStream)
                outputStream.close()
                

    
